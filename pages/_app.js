import Footer from "../src/components/Footer/Footer";
import Navbar from "../src/components/Navbar";
import "antd/dist/antd.css";
import "../styles/globals.css";
import TransitionLayout from "../src/components/TransitionLayout";

function MyApp({ Component, pageProps }) {
  return (
    <div>
      <Navbar />
      <TransitionLayout>
        <div className={"pt-[78px]"}>
          <Component {...pageProps} />
        </div>
      </TransitionLayout>
      <Footer type={"icon"} textClassname={"text-[14px] text-white"} />
    </div>
  );
}

export default MyApp;
