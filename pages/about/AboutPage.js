import Image from "next/image";
import IntroductionCard from "../../src/components/IntroductionCard";
import Partner from "../../src/components/Partner/Partner";
import Meta from "../../src/components/Meta";

export default function AboutPage() {
  return (
    <>
      <Meta title={"БИДНИЙ ТУХАЙ"} />
      <div className={"bg-[rgb(250,250,250)]"}>
        <div className={"flex flex-col md:py-[40px] py-[20px] px-[20px] md:px-[60px] min-h-[600px]"}>
          <div className={"relative min-h-[499px] max-h-[499px] h-full w-full"}>
            <Image
              priority={true}
              quality={100}
              objectFit={"cover"}
              layout={"fill"}
              alt="PickPack warehouse"
              src={"/assets/images/cover.jpeg"}
            />
          </div>
          <IntroductionCard />
          <section className={"max-w-[980px] mx-auto mt-[40px] md:mt-[60px] w-full"}>
            <Partner />
          </section>
        </div>
      </div>
    </>

  );
}
