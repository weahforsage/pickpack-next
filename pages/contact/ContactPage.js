import Meta from "../../src/components/Meta";

const ContactPage = () => {
  return (
    <>
      <Meta title={"ХОЛБОО БАРИХ"} />
      <div>
        <div
          style={{
            backgroundImage: "url(/assets/images/contact-cover.jpeg)",
            backgroundPosition: "center",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
          }}
          className={"relative w-full bg-scroll md:bg-fixed min-h-[446px]"}
        />
        <div
          className={
            "relative max-w-[980px] bg-[rgb(246,246,246)] top-[calc(-505px/2)] min-h-[505px] mx-auto p-[20px] md:p-[40px] md:pl-[70px]"
          }
        >
          <div
            className={
              "grid grid-cols-1 md:grid-cols-2 place-items-center gap-[20px] justify-between items-center"
            }
          >
            <div className={"grid gap-[20px] flex-1 max-w-[350px]"}>
              <h1 className={"text-[36px] text-[#324158] font-bold"}>
                Бидэнтэй холбогдох
              </h1>
              <p className={"text-[16px] leading-[25px] text-[rgb(98,98,98)]"}>
                Бидэнтэй холбогдох Хамтран ажиллах, үйлчилгээний талаар
                дэлгэрэнгүй мэдээлэл, агуулахаар зочилохоор бол бидэнтэй
                холбогдоорой.
              </p>
              <div
                className={
                  "grid grid-cols-[repeat(auto-fill,minmax(100px,1fr))]"
                }
              >
                <div className={"flex flex-col items-start"}>
                  <p className={"font-bold text-[#324158]"}>Утас:</p>
                  <p className={"text-[#626262]"}>7777-6090</p>
                </div>
                <div className={"flex flex-col items-start"}>
                  <p className={"font-bold text-[#324158]"}>И-мэйл:</p>
                  <p className={"text-[#626262]"}>info@pickpack.mn</p>
                </div>
              </div>
              <div className={"flex flex-col items-start"}>
                <p className={"font-bold text-[#324158]"}>Цагийн хуваарь:</p>
                <p className={"text-[#626262]"}>Даваа-Ням 10:00-19:00</p>
              </div>
              <div className={"flex flex-col items-start"}>
                <p className={"font-bold text-[#324158]"}>PickPack агуулах:</p>
                <p className={"text-[#626262]"}>
                  ХУД, 2-р хороо, Зайсангийн гудамж, Оргил Шилтгээний урд
                </p>
              </div>
            </div>
            <div
              className={"w-full h-full flex items-center max-w-[400px] flex-1"}
            >
              <video controls loop autoPlay muted>
                <source src={"/assets/videos/map.mp4"} type="video/mp4" />
                Your browser does not support the video tag.
              </video>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ContactPage;
