import HeroHeader from "../src/components/HeroHeader";
import ImageCard from "../src/components/ImageCard";
import ServicesSection from "../src/components/ServicesSection";
import Statistics from "../src/components/Statistics/Statistics";
import angilalt from "../public/assets/images/angilalt-edited.jpg";
import PricingBanner from "../src/components/PricingBanner";
import VideoCard from "../src/components/VideoCard";
import BoxBanner from "../src/components/BoxBanner";
import Meta from "/src/components/Meta";

export default function Home() {
  return (
    <div>
      <Meta title={"НҮҮР"} />
      {/* Main section */}
      <main>
        <HeroHeader />
        <ServicesSection />
        <Statistics />
        <section>
          <ImageCard
            buttonRoute={"/services"}
            image={angilalt}
            lists={[
              "Бараа, бүтээгдэхүүнийг таны хүссэн газраас очин татан авалт хийнэ",
              "Захиалга ангилах, бэлтгэх, ажлаа манай мэргэжлийн багт даатгаарай",
            ]}
            title={"Мэргэжлийн баг"}
            type={"left"}
          />
        </section>
        <PricingBanner />
        <VideoCard
          buttonRoute={"/services"}
          videoClass={
            "max-w-[640px] min-w-[320px] w-full h-full max-h-[375px] flex-1"
          }
          videoUrl={"/assets/videos/web-motion-delivery.mp4"}
          title={"Өдөртөө багтах хүргэлт"}
          lists={[
            "Хүргэлтийн төлвөө системээр хянах боломжтой",
            "B2B түгээлтийн үйлчилгээ",
          ]}
        />
        <BoxBanner />
        <VideoCard
          buttonRoute={"/pp-box"}
          videoClass={
            "max-w-[640px] min-w-[320px] w-full h-full max-h-[375px] flex-1"
          }
          videoUrl={"/assets/videos/file.mp4"}
          title={"АВ, ХАДГАЛ, ИЛГЭЭ"}
          lists={[
            "Хотын 200 байршилд ав, хадгал, илгээ",
            "Апп-аа ашиглан өөрт ойр байршлуудыг хараарай",
          ]}
        />
      </main>
    </div>
  );
}
