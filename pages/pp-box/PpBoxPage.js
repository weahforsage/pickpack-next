import PpBoxCarousel from "../../src/components/PpBox/PpBoxCarousel";
import Meta from "../../src/components/Meta";
import PpBoxBackground from "../../src/components/PpBoxBackground";
import PpBoxCard1 from "../../src/components/PpBox/PpBoxCard1";
import PpBoxCard2 from "../../src/components/PpBox/PpBoxCard2";
import PpBoxCard3 from "../../src/components/PpBox/PpBoxCard3";

export default function PpBoxPage() {
  return (
    <>
      <Meta title={"PP BOX"} />
      <div>
        <PpBoxCarousel />
        <div
          className={"ppBoxBgGradient relative w-full h-full overflow-hidden"}
        >
          <PpBoxBackground />

          <div
            className={
              "max-w-[800px] px-[20px] mx-auto w-full h-full py-[20px] md:py-[80px]"
            }
          >
            <PpBoxCard1 />
            <PpBoxCard2 />
            <PpBoxCard3 />
          </div>
        </div>
      </div>
    </>
  );
}
