import Image from "next/image";
import ContactForm from "../../src/components/ContactForm";
import Head from "next/head";
import Meta from "../../src/components/Meta";

export default function PricingPage() {
  return (
    <>
      <Meta title={"ҮНЭ"} />
      <div className={"bg-[rgb(250,250,250)]"}>
        <div className={"min-h-[446px]"}>
          <div className={"relative min-h-[446px] max-h-[446px] h-full w-full"}>
            <Image
              priority={true}
              quality={100}
              objectFit={"cover"}
              layout={"fill"}
              alt="PickPack warehouse"
              src={"/assets/images/pricing-cover.jpeg"}
            />
          </div>
        </div>
        <div
          className={
            "lg:top-[calc(-580px/2)] max-w-[980px] lg:h-[calc(580px/2)] mx-auto relative w-full"
          }
        >
          <div
            className={
              "grid grid-cols-1 md:grid-cols-2 gap-[20px] place-items-center md:flex-row items-center md:items-start justify-between bg-[rgb(246,246,246)] p-[20px] md:p-[50px]"
            }
          >
            <div className={"grid max-w-[410px] gap-[20px] py-[20px] md:py-[40px]"}>
              <h1
                className={
                  "text-[36px] text-[#324158] font-bold leading-[43px]"
                }
              >
                Үнийн санал авах
              </h1>
              <p className={"text-[16px] leading-[1.8em] text-[#626262]"}>
                Өөрийн бизнест тохирох үйлчилгээ болон үнийн саналыг бидэнтэй
                холбогдон аваарай.
              </p>
              <div className={"grid grid-cols-[140px,140px]"}>
                <div className={"flex flex-col items-start"}>
                  <p className={"font-bold text-[#324158]"}>Утас:</p>
                  <p className={"text-[#626262]"}>7777-6090</p>
                </div>
                <div className={"flex flex-col items-start"}>
                  <p className={"font-bold text-[#324158]"}>И-мэйл:</p>
                  <p className={"text-[#626262]"}>info@pickpack.mn</p>
                </div>
              </div>

            </div>
            <ContactForm />
          </div>
        </div>
      </div>
    </>
  );
}
