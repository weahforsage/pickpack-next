import ColorCard from "../../src/components/ColorCard/ColorCard";
import { cards } from "../../src/utils/consts";
import ContactForm from "../../src/components/ContactForm/ContactForm";
import Meta from "../../src/components/Meta";

export default function ServicesPage() {
  return (
    <>
      <Meta title={"ҮЙЛЧИЛГЭЭ"} />
      <div className={"bg-[rgb(250,250,250)]"}>
        <div className={"flex flex-col items-center min-h-[600px]"}>
          <div
            style={{
              backgroundImage: "url(/assets/images/services_cover.webp)",
              backgroundPosition: "center",
              backgroundRepeat: "no-repeat",
            }}
            className={"relative bg-scroll md:bg-fixed min-h-[463px] max-h-[463px] h-full w-full"}
          />
          <div
            className={
              "grid p-[20px] sm:p-0 gap-[20px] max-w-[366px] items-center justify-center w-full mt-[40px] mb-[20px]"
            }
          >
            <h1
              className={
                "text-[32px] text-[#324158] font-bold text-center leading-[38px]"
              }
            >
              ӨӨРӨӨ БҮГДИЙГ БИШ PICKPACK БҮГДИЙГ
            </h1>
            <p className={"text-[16px] text-[rgb(98,98,98)] leading-[1.8em]"}>
              Таны бизнесийн онцлогт тохирох ложистикийн төрөл бүрийн үйлчилгээг
              бид санал болгож байна. Хэрвээ танд зөвлөгөө хэрэгтэй байвал
              эргэлзэлгүй бидэнтэй шууд холбогдоорой. Бид туслахад хэзээд бэлэн!
            </p>
          </div>
          <div
            className={
              "max-w-[960px] mt-[10px] justify-center mx-auto w-full gap-[10px] grid grid-cols-[repeat(auto-fit,minmax(313px,1fr))] mb-[20px]"
            }
          >
            {cards.map((card) => {
              return <ColorCard data={card} key={card.id} />;
            })}
            <div
              className={"flex items-center justify-center md:w-[616px] md:h-[369px] p-[20px]"}
            >
              <h1
                className={
                  "text-pickpack-orange text-[20px] md:text-[26px] font-bold max-w-[413px]"
                }
              >
                ЯМАР ҮЙЛЧИЛГЭЭ ТАНД ТОХИРОХЫГ МЭДЭХГҮЙ БАЙВАЛ БИДЭНТЭЙ ХОЛБОГДОН
                ЗӨВЛӨГӨӨ АВААРАЙ.
              </h1>
            </div>
          </div>
          <section className={"bg-[#f6f6f6] w-full mt-[40px] p-[40px]"}>
            <div
              className={
                "grid grid-cols-1 gap-[20px] md:grid-cols-2 w-full max-w-[900px] place-items-center mx-auto items-center justify-center md:justify-between "
              }
            >
              <div
                className={"flex flex-col max-w-[330px] mx-auto justify-center"}
              >
                <h1
                  className={
                    "text-[36px] text-[#324158] font-bold leading-[43px]"
                  }
                >
                  Үнийн санал авах
                </h1>
                <p className={"text-[16px] text-[rgb(98,98,98)] mt-[10px]"}>
                  Та сонирхож буй үйлчилгээний үнэ болон дэлгэрэнгүй мэдээллийг
                  аваарай.
                </p>

              </div>
              <ContactForm />
            </div>
          </section>
        </div>
      </div>
    </>
  );
}
