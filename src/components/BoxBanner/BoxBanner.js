import Image from "next/image";
import {useInView} from "react-intersection-observer";

export default function BoxBanner() {
  const { ref, inView } = useInView({
    threshold: 0.1,
    triggerOnce: true,
  });
  return (
    <section ref={ref} className={"relative"}>
      <Image
        layout={"fill"}
        src={"/assets/images/pp-background-01.png"}
        alt={"PP Box background"}
      />
      <div
        className={
          "min-h-[492px] relative flex items-row items-center justify-center px-[20px] lg:px-[100px]"
        }
      >
        <div className={`${inView ? "opacity-100 translate-x-0" : "opacity-0 translate-x-[-100px]"} transition-all duration-[2000ms] relative flex flex-row items-center h-full`}>
          <Image
            className={"max-h-[450px] object-cover"}
            layout={"raw"}
            sizes={"450"}
            width={216}
            height={450}
            src={"/assets/images/ppbox.png"}
            alt={"PP Box"}
          />
          <div className={"relative hidden lg:flex"}>
            <div className={"relative"}>
              <Image
                sizes={"450"}
                layout={"raw"}
                className={"max-h-[450px] object-cover"}
                width={216}
                height={450}
                src={"/assets/images/ppbox.png"}
                alt={"PP Box"}
              />
            </div>

            <div className={"relative right-[55px]"}>
              <Image
                sizes={"450"}
                className={"max-h-[450px] object-cover"}
                layout={"raw"}
                width={216}
                height={450}
                src={"/assets/images/ppbox.png"}
                alt={"PP Box"}
              />
            </div>
            <div className={"relative right-[120px] top-[4px]"}>
              <Image
                sizes={"450"}
                layout={"raw"}
                className={"max-h-[450px] object-cover"}
                width={216}
                height={450}
                src={"/assets/images/ppbox.png"}
                alt={"PP Box"}
              />
            </div>
          </div>
        </div>
        <h1 className={"text-[20px]  md:text-[46px] md:leading-[56px] text-white font-bold"}>Таны шинэ <br/> 24/7 хаяг</h1>
      </div>
    </section>
  );
}
