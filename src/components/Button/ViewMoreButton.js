export default function ViewMoreButton({ className, children, ...props }) {
  return (
    <button
      className={`transition-all duration-300 hover:shadow-[0_0_6px_1px_rgb(95_91_205)] px-[16px] rounded-[50px] text-[16px] w-max min-w-[137px] text-white h-[37px] shadow-[0_0_6px_0_rgb(64_145_175/70%)] ${
        className ? className : ""
      }`}
      {...props}
    >
      {children}
    </button>
  );
}
