import Image from "next/image";
export default function ColorCard({ data }) {
  return (
    <div
      className={
        "flex flex-col items-center min-h-[396px] min-w-[313px] relative group"
      }
    >
      <div
        className={
          "relative min-h-[396px] min-w-[313px] w-full h-full bg-blue-50 overflow-hidden"
        }
      >
        <Image
          alt={""}
          layout={"fill"}
          objectFit={"cover"}
          src={data.imagePath}
        />
        <div
          className={`absolute bottom-0 ${data.color} transition-[height] duration-500 group-hover:h-[147px] h-[102px] w-full text-white p-[20px]`}
        >
          <h1
            className={
              "transition-all text-white duration-500 text-[20px] font-bold"
            }
          >
            {data.title}
          </h1>
          <p
            className={
              "opacity-0 group-hover:opacity-100 group-hover:translate-y-0 transition-all duration-700 text-[14px] leading-[18px] mt-[6px]"
            }
          >
            {data.description}
          </p>
          <p
            className={
              "text-[14px] font-light absolute top-[50px] group-hover:translate-y-[-40px]  group-hover:opacity-0 transition-all duration-300"
            }
          >
            Дэлгэрэнгүй...
          </p>
        </div>
      </div>
    </div>
  );
}
