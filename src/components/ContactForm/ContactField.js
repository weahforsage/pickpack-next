export default function ContactField({
  containerClass,
  label,
  labelClass,
  inputClass,
  inputType,
  ...props
}) {
  return (
    <div className={`flex flex-col ${containerClass ? containerClass : ""}`}>
      <label
        className={`text-[14px] ${
          props.required
            ? "after:content-['*'] after:ml-[2px] after:text-blue-400"
            : ""
        } text-[rgb(50,65,88)] leading-[1.4em] mb-[4px] font-medium ${
          labelClass ? labelClass : ""
        }`}
      >
        {label}
      </label>
      {inputType === "textarea" ? (
        <textarea
          className={`transition-all duration-150 border-[1px] hover:border-[rgb(163,217,246)] hover:bg-[rgb(234,247,255)] px-[10px] outline-none focus-visible:border-[rgb(163,217,246)] border-transparent bg-white rounded-[6px] h-[37px] ${
            inputClass ? inputClass : ""
          }`}
          {...props}
        />
      ) : (
        <input
          className={`transition-all duration-150 border-[1px] hover:border-[rgb(163,217,246)] hover:bg-[rgb(234,247,255)] px-[10px] outline-none focus-visible:border-[rgb(163,217,246)] border-transparent bg-white rounded-[6px] h-[37px] invalid:border-red-400 ${
            inputClass ? inputClass : ""
          }`}
          {...props}
        />
      )}
    </div>
  );
}
