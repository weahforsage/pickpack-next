import ContactField from "./ContactField";
import Dropdown from "./Dropdown";
import {useState} from "react";

export default function ContactForm() {
  const [formData, setFormData] = useState({
    name: "",
    company: "",
    phoneNumber: "",
    email: "",
    service: "",
    message: "",
  });

  const handleChange = ({ event, key }) => {
    setFormData((prev) => ({ ...prev, [key]: event.target.value }));
  };

  return (
    <div className={"flex flex-col max-w-[421px] h-full w-full"}>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          console.log(formData);
        }}
      >
        <div className={"flex flex-col md:flex-row"}>
          <ContactField
            containerClass={"mr-0 last:mr-0 md:mr-[20px] md:w-1/2"}
            type={"text"}
            onChange={(e) => handleChange({ event: e, key: "name" })}
            value={formData.name}
            required
            label={"Нэр"}
          />
          <ContactField
            containerClass={"md:w-1/2"}
            type={"text"}
            onChange={(e) => handleChange({ event: e, key: "company" })}
            value={formData.company}
            required
            label={"Байгууллага"}
          />
        </div>
        <div className={"mt-[10px]"}>
          <ContactField
            onInvalid={(e) => {
              e.target.setCustomValidity(
                "Та утасны дугаар аа шалгана уу.\n Боломжит хувилбар: +976-9******, +9769****** 9******"
              );
            }}
            type={"tel"}
            maxLength={20}
            pattern={"^([+][0-9]{3}([-]){0,1})?[0-9]{8}$"}
            value={formData.phoneNumber}
            onChange={(e) => handleChange({ event: e, key: "phoneNumber" })}
            required
            label={"Утас"}
          />
        </div>
        <div className={"mt-[10px]"}>
          <ContactField
            type={"email"}
            value={formData.email}
            onChange={(e) => handleChange({ event: e, key: "email" })}
            required
            label={"И-Мэйл"}
          />
        </div>
        <div className={"mt-[10px]"}>
          <Dropdown
            defaultValue={formData.service ? formData.service : ""}
            onChange={(e) => handleChange({ event: e, key: "service" })}
            required
            label={"Сонирхож буй үйлчилгээ"}
          />
        </div>
        <div className={"mt-[10px]"}>
          <ContactField
            type={"text"}
            value={formData.message}
            onChange={(e) => handleChange({ event: e, key: "message" })}
            inputType={"textarea"}
            inputClass={"py-[10px] resize-none min-h-[86px]"}
            label={"Мессеж"}
          />
        </div>
        <div className={"w-full flex items-center justify-center mt-[20px]"}>
          <input
            className={
              "cursor-pointer bg-pickpack-orange rounded-full text-[16px] h-[36px] w-[142px] leading-[1.4em] font-bold text-white py-[6px] px-[20px]"
            }
            type={"submit"}
            value={"Илгээх "}
          />
        </div>
      </form>
    </div>
  );
}
