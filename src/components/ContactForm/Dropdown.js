export default function Dropdown({ label, labelClass, ...props }) {
  return (
    <div className={"flex flex-col"}>
      <label
        className={`text-[14px] ${
          props.required
            ? "after:content-['*'] after:ml-[2px] after:text-blue-400"
            : ""
        } text-[rgb(50,65,88)] leading-[1.4em] mb-[4px] font-medium ${
          labelClass ? labelClass : ""
        }`}
      >
        {label}
      </label>
      <select
        className={
          "text-[14px] text-blue-400 transition-all duration-150 border-[1px] hover:border-[rgb(163,217,246)] hover:bg-[rgb(234,247,255)] px-[10px] outline-none focus-visible:border-[rgb(163,217,246)] border-transparent bg-white rounded-[6px] h-[37px]"
        }
        {...props}
      >
        <option hidden value={""} disabled> Сонгох</option>
        <option>Стандарт үйлчилгээ</option>
        <option>Агуулахын үйлчилгээ</option>
        <option>Хүргэлтийн үйлчилгээ</option>
        <option>B2B</option>
        <option>PP Box</option>
      </select>
    </div>
  );
}
