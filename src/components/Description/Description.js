import { CheckCircleFilled } from "@ant-design/icons";
import ViewMoreButton from "../Button/ViewMoreButton";
import Link from "next/link";

export default function Description({ title, lists, buttonRoute, inView }) {
  return (
    <div
      className={`${
        inView ? "opacity-100 translate-y-0" : "opacity-0 translate-y-[100px]"
      } transition-all duration-[2000ms] flex flex-col justify-center flex-1`}
    >
      <h1
        className={
          "max-w-[360px] text-[24px] md:text-[32px] md:leading-[38px] font-bold uppercase text-[#324158]"
        }
      >
        {title}
      </h1>
      {lists && (
        <div
          className={
            "flex flex-col text-[14px] my-[14px] md:my-[28px] leading-[18px] text-[rgb(98,98,98)]"
          }
        >
          {lists.map((item, index) => {
            return (
              <div key={index} className={"grid my-[5px] grid-cols-[1fr_100%]"}>
                <CheckCircleFilled
                  className={"text-pickpack-orange mt-[6px]"}
                  size={14}
                />
                <span className={"ml-[6px] text-[16px] leading-[1.5em]"}>
                  {item}
                </span>
              </div>
            );
          })}
        </div>
      )}
      <Link href={`${buttonRoute}`}>
        <a>
          <ViewMoreButton className={"bg-[#40C0F2]"}>
            Дэлгэрэнгүй
          </ViewMoreButton>
        </a>
      </Link>
    </div>
  );
}
