import Image from "next/image";
import FooterText from "./FooterText";

export default function Footer({ textClassname }) {
  return (
    <footer>
      <div
        className={
          "flex flex-col items-center justify-between min-h-[320px] gap-[20px] bg-[#324158] p-[30px]"
        }
      >
        <div
          className={
            "flex flex-row items-start justify-between w-full max-w-[768px] mx-auto"
          }
        >
          <div className={"flex flex-col items-start gap-[10px] w-full"}>
            <Image
              width={48}
              height={48}
              alt={"pickpack logo"}
              src={"/assets/images/logo_image.svg"}
            />
            <h1 className={"text-[20px] text-white font-bold uppercase"}>
              Бидэнтэй холбогдох
            </h1>
            <FooterText type={"icon"} className={textClassname} />
          </div>
          <div
            className={"flex flex-row items-end justify-end w-full gap-[10px]"}
          >
            <a
              target={"_blank"}
              rel={"noreferrer"}
              href={"https://www.facebook.com/PickPackFulfillment"}
            >
              <Image
                layout={"raw"}
                height={39}
                width={39}
                alt={"Facebook Logo"}
                src={"/assets/images/svg_facebook.svg"}
              />
            </a>
            <a
              target={"_blank"}
              rel={"noreferrer"}
              href={"https://www.instagram.com/pickpack.llc/"}
            >
              <Image
                layout={"raw"}
                height={39}
                width={39}
                alt={"Facebook Logo"}
                src={"/assets/images/svg_instagram.svg"}
              />
            </a>
          </div>
        </div>

        <p className={"text-[14px] text-white font-semibold"}>
          PickPack © 2022
        </p>
      </div>
    </footer>
  );
}
