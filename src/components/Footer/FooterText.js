import Image from "next/image";
export default function FooterText({ className, type }) {
  return (
    <ul
      className={`${
        className ? className : ""
      } grid gap-[10px] max-w-[250px]`}
    >
      <li>
        <div className={"grid items-start"}>
          {type === "icon" ? (
            <div className={"mr-[10px] mt-[4px]"}>
              <Image
                height={15}
                width={15}
                alt={"phone icon"}
                src={"/assets/images/phone.svg"}
              />
            </div>
          ) : (
            <p className={"mr-[5px]"}>Утас:</p>
          )}

          <p>7777-6090</p>
        </div>
      </li>
      <li>
        <div className={"grid items-start"}>
          {type === "icon" ? (
            <div className={"mr-[10px] mt-[4px]"}>
              <Image
                height={15}
                width={15}
                alt={"mail icon"}
                src={"/assets/images/mail.svg"}
              />
            </div>
          ) : (
            <p className={"mr-[5px]"}>И-мэйл:</p>
          )}
          <p>info@pickpack.mn</p>
        </div>
      </li>
      <li>
        <div className={"grid items-start"}>
          {type === "icon" && (
            <div className={"mr-[10px] mt-[4px]"}>
              <Image
                className={"mt-[4px]"}
                height={40}
                width={40}
                alt={"location icon"}
                src={"/assets/images/location.svg"}
              />
            </div>
          )}
          <p>
            Монгол улс, Улаанбаатар хот, СБД, 1-р хороо, Чингисийн өргөн чөлөө,
            Централ Парк, 14 давхар
          </p>
        </div>
      </li>
    </ul>
  );
}
