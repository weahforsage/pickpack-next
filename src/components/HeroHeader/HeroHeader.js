export default function HeroHeader() {
  return (
    <section className={"heroHeader bg-scroll md:bg-fixed h-[580px] w-full px-[clamp(10px,10%,188px)]"}>
      <div
        className={
          "flex flex-col justify-center relative h-full max-w-[398px]"
        }
      >
        <h3
          className={
            "flex flex-col items-start text-[30px] md:text-[46px] md:leading-[56px] text-white font-bold"
          }
        >
          <span>Сайн уу? <br/>PickPack байна.</span>
        </h3>
        <p className={"text-[14px] text-white text-justify leading-[1.7em] mt-[26px]"}>
          Бид худалдааны бизнес эрхлэгчдэд зориулан Логистик болон Ханган
          нийлүүлэлтийн цогц үйлчилгээ үзүүлдэг байгууллага бөгөөд 110 гаруй
          байгууллага харилцагчиддаа технологид суурилсан шийдэл санал болгон
          хамтран ажиллаж байна.
        </p>
      </div>
    </section>
  );
}
