import Image from "next/image";
import Description from "../Description/Description";
import { useInView } from "react-intersection-observer";

export default function ImageCard({ type, title, lists, buttonRoute, image }) {
  const { ref, inView } = useInView({
    threshold: 0.1,
    triggerOnce: true,
  });

  return (
    <div
      ref={ref}
      className={`overflow-hidden flex w-full h-full p-[20px] max-w-[1100px] mx-auto ${
        type === "right"
          ? "flex-col md:flex-row justify-center"
          : "flex-col md:flex-row-reverse justify-end"
      } flex`}
    >
      <div
        className={`flex items-center ${
          type === "right" ? "lg:mr-[40px] mr-[20px]" : "ml-[20px] lg:ml-[40px]"
        } mt-[40px] lg:mt-0`}
      >
        <Description
          inView={inView}
          lists={lists}
          title={title}
          buttonRoute={buttonRoute}
        />
      </div>

      <div
        className={`${
          inView ? "opacity-100 translate-x-0" : "opacity-0 translate-x-[100px]"
        } mt-[20px] lg:mt-0 transition-all duration-[1500ms] relative self-center max-w-[638px] w-full max-h-[425px] h-full`}
      >
        <Image alt="angilalt" src={image} />
      </div>
    </div>
  );
}
