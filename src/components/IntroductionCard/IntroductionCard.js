import Image from "next/image";

export default function IntroductionCard() {
  return (
    <div
      className={
        "grid grid-cols-1 md:grid-cols-2 place-items-center items-center mt-[20px] md:mt-[60px] max-w-[980px] w-full mx-auto gap-[60px]"
      }
    >
      <div
        className={"order-2 md:order-1 relative min-h-[600px] h-[600px] w-full max-w-[500px] aspect-[1/2]"}
      >
        <Image
          priority={true}
          objectFit="cover"
          alt=""
          src={"/assets/images/about2.jpeg"}
          layout={"fill"}
        />
      </div>
      <div className={"order-1 md:order-2 grid items-start gap-[10px] md:gap-[30px] justify-center max-w-[440px]"}>
        <h1 className={"text-[44px] font-bold uppercase text-[#324158] h-full"}>
          Танилцъя
        </h1>
        <div
          className={
            "grid justify-center gap-[30px] leading-[1.8em] text-[16px] text-[rgb(98,98,98)]"
          }
        >
          <p>
            PickPack нь Монгол орон даяарх хүргэлт, түгээлтийн дэд бүтэц бүхий
            технологийн шийдэлд суурилсан 3PL ЛОЖИСТИКИЙН КОМПАНИ юм. Бид
            хүргэлтийн үйлчилгээ хэрэгцээтэй хувь хүмүүсээс эхлээд борлуулалтаа
            тэлэхээр зорьж буй бүхий л төрлийн худалдааны бизнес эрхлэгч хүртэлх
            бүх шатны хэрэглэгчдэд АГУУЛАХ, АНГИЛАЛТ, ХҮРГЭЛТ-ийн цогц
            үйлчилгээг үзүүлдэг билээ.
          </p>
          <p>
            Мөн харилцагч, үйлчлүүлэгчдэдээ илүү ойроос хүрч үйлчлэх зорилгоор
            УБ хотод “PickPack Main Fulfillment Center”, УБ хотын 4 цэгт байрших
            “PickPack Hub Center”, 21 аймагт 30 “PickPack Local Fulfillment
            Center”, орон даяар 400 ширхэг “PP Box”-ыг хамарсан үндэсний
            ложистикийн дэд бүтцийг цогцлоогоод байна.
          </p>
        </div>
      </div>
    </div>
  );
}
