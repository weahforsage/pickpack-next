import Head from "next/head";
import { useRouter } from "next/router";

const Meta = ({ title }) => {
  const { pathname } = useRouter();
  return (
    <Head>
      <title>{`${title} | PickPack`}</title>
      <meta property="og:title" content={`${title} | PickPack`} />
      <meta property="og:url" content={`https://www.pickpack.mn${pathname}`} />
      <meta property="og:site_name" content="PickPack" />
      <meta property="og:type" content="website" />
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:title" content={`${title} | PickPack`} />
      <link rel="icon" href="/favicon.ico" />
    </Head>
  );
};

export default Meta;
