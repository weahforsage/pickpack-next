import { menuData } from "../../../utils/consts";
import Link from "next/link";
import { useRouter } from "next/router";

export default function MenuItems({ type }) {
  const { pathname } = useRouter();
  return (
    <div
      className={`flex ${
        type === "desktop" ? "flex-row items-center gap-[30px]" : "flex-col gap-[10px]"
      }`}
    >
      <ul className={`flex uppercase font-medium ${type === "desktop" ? "flex-row gap-[30px]" : "flex-col"}`}>
        {menuData.map((menu) => {
          return (
            <li key={menu.id}>
              <Link href={menu.route}>
                <a>
                  <p
                    className={`${
                      pathname === menu.route ? "text-[rgb(2,153,199)]" : ""
                    } transition-all duration-200 text-[14px] leading-[49px] hover:text-[rgb(2,153,199)]`}
                  >
                    {menu.title}
                  </p>
                </a>
              </Link>
            </li>
          );
        })}
      </ul>
      <Link href={"https://business.cody.mn/"}>
        <a target={"_blank"} rel={"referrer"}>
          <button
            className={
              "h-[32px] transition-all duration-200 hover:bg-white hover:text-black hover:border-black flex items-center font-semibold px-[10px] bg-[rgb(144,122,207)] text-white uppercase rounded-[300px] text-[14px] leading-[1.4em] border-[2px] border-[rgb(144,122,207)]"
            }
          >
            Нэвтрэх
          </button>
        </a>
      </Link>
    </div>
  );
}
