import { CloseOutlined } from "@ant-design/icons";
import Link from "next/link";
import Image from "next/image";
import MenuItems from "./MenuItems";
import { Drawer } from "antd";

const MobileDrawer = ({ setIsOpen, isOpen }) => {
  return (
    <Drawer
      contentWrapperStyle={{
        maxWidth: "320px",
        width: "100%",
      }}
      extra={
        <CloseOutlined
          onClick={() => setIsOpen(false)}
          className={"text-[18px]"}
        />
      }
      title={
        <Link href={`/`}>
          <a className={"relative"}>
            <Image
              layout={"fixed"}
              height={55}
              width={175}
              alt={"PickPack Logo"}
              className={"object-cover"}
              src={"/assets/images/pickpack_logo.png"}
            />
          </a>
        </Link>
      }
      placement={"left"}
      closable={false}
      visible={isOpen}
      onClose={() => setIsOpen(!isOpen)}
      key={"left"}
    >
      <div onClick={()=> setIsOpen(false)}>
        <MenuItems type={"mobile"} />
      </div>
    </Drawer>
  );
};

export default MobileDrawer;
