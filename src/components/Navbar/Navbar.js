import Image from "next/image";
import MenuItems from "./MenuItems/MenuItems";
import Link from "next/link";
import useMediaQuery from "../../hooks/useMediaQuery";
import { MenuOutlined } from "@ant-design/icons";
import MobileDrawer from "./MobileDrawer";
import { useState } from "react";

export const Navbar = () => {
  const [isMobileMenuOpen, setIsMobileMenuOpen] = useState(false);
  const isSmallScreen = useMediaQuery("screen and (max-device-width: 900px)");

  return (
    <>
      {isSmallScreen && (
        <MobileDrawer
          isOpen={isMobileMenuOpen}
          setIsOpen={setIsMobileMenuOpen}
        />
      )}
      <header
        className={
          "fixed top-0 h-[78px] px-[20px] md:px-[50px] w-full bg-white p-[10px] z-[1] shadow-[0_0_5px_rgba(0,0,0,.5)]"
        }
      >
        <div
          className={
            "flex flex-row items-center w-full h-full justify-between max-w-[980px] mx-auto"
          }
        >
          <div
            className={
              "flex items-center justify-center relative w-[175px] h-[55px] flex-shrink-0"
            }
          >
            {isSmallScreen && (
              <div
                className={
                  "flex items-center justify-center w-[20px] h-[20px] mr-[10px]"
                }
                onClick={() => setIsMobileMenuOpen(true)}
              >
                <MenuOutlined className={"text-[20px]"} />
              </div>
            )}

            <Link href={`/`}>
              <a className={"relative w-full h-full"}>
                <Image
                  priority={true}
                  quality={100}
                  layout={"fill"}
                  alt={"PickPack Logo"}
                  className={"object-cover"}
                  src={"/assets/images/pickpack_logo.png"}
                />
              </a>
            </Link>
          </div>
          {!isSmallScreen && <MenuItems type={"desktop"} />}
        </div>
      </header>
    </>
  );
};
