import ContactForm from "../ContactForm";
import FooterText from "../Footer/FooterText";
import { FacebookOutlined, InstagramOutlined } from "@ant-design/icons";

export default function Partner() {
  return (
    <div
      className={
        "gap-[20px] grid grid-cols-1 md:grid-cols-2 place-items-center items-center justify-between"
      }
    >
      <div className={"grid max-w-[410px]"}>
        <h1 className={"text-[36px] text-[#324158] font-bold leading-[43px]"}>
          Хамтран ажилламаар байна уу?
        </h1>
        <FooterText
          type={"text"}
          className={"text-[#626262] mt-[20px] text-[16px]"}
        />
        <div
          className={
            "grid grid-cols-[repeat(auto-fill,minmax(20px,1fr))] items-center gap-[10px] mt-[20px]"
          }
        >
          <a
            rel={"noreferrer"}
            target={"_blank"}
            href={"https://www.facebook.com/PickPackFulfillment"}
          >
            <FacebookOutlined className={"text-[20px]"} />
          </a>
          <a
            rel={"noreferrer"}
            target={"_blank"}
            href={"https://www.instagram.com/pickpack.llc"}
          >
            <InstagramOutlined className={"text-[20px]"} />
          </a>
        </div>
      </div>
      <ContactForm />
    </div>
  );
}
