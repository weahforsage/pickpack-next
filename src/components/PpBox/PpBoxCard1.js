import Image from "next/image";
import { CheckCircleFilled } from "@ant-design/icons";
import { useInView } from "react-intersection-observer";

const PpBoxCard1 = () => {
  const { ref, inView } = useInView({
    threshold: 0.1,
    triggerOnce: true,
  });
  const lists = [
    {
      id: 0,
      title: "Захиалсан бараагаа авах завгүй үед",
    },
    {
      id: 1,
      title: "Гэрийн хаягаа зааж өгмөөргүй байвал",
    },
    {
      id: 2,
      title: "Таалагдаагүй, таараагүй бараагаа буцаамаар байвал",
    },
    {
      id: 3,
      title: "Хүргэлтийн хүнээс захиалсан бараагаа авмааргүй үед",
    },
    {
      id: 4,
      title: "Мартсан санаснаа хүргэж өгч амжихгүй үед",
    },
    {
      id: 5,
      title: "Гадуур олон зүйл барьж явмааргүй байвал",
    },
  ];
  return (
    <div
      ref={ref}
      className={
        "grid md:grid-cols-2 grid-cols-1 item-center gap-[20px] justify-center"
      }
    >
      <Image
        className={`transition-all duration-1000 ${
          inView
            ? "opacity-100 translate-x-0"
            : "opacity-0 translate-x-[-100px]"
        }`}
        alt={""}
        width={342}
        height={342}
        src={"/assets/images/ppBoxGuide.svg"}
      />
      <div
        className={`${
          inView ? "opacity-100" : "opacity-0"
        } transition-all duration-[2000ms] flex flex-col items-center justify-center`}
      >
        <h1 className={"md:self-start text-white font-bold text-[30px] mb-[20px]"}>
          ТАНЫ ШИНЭ 24/7 ХАЯГ
        </h1>
        {lists && (
          <div
            className={
              "grid gap-[4px] text-[14px] leading-[18px] text-[rgb(98,98,98)]"
            }
          >
            {lists.map((item, index) => {
              return (
                <div key={item.id} className={"grid grid-cols-[1fr_100%]"}>
                  <CheckCircleFilled
                    className={"text-white mt-[4px]"}
                    size={14}
                  />
                  <span
                    className={
                      "ml-[6px] text-white text-[16px] leading-[1.5em]"
                    }
                  >
                    {item.title}
                  </span>
                </div>
              );
            })}
          </div>
        )}
      </div>
    </div>
  );
};

export default PpBoxCard1;
