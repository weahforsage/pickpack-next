import Image from "next/image";
import { CheckCircleFilled } from "@ant-design/icons";
import { useInView } from "react-intersection-observer";

const PpBoxCard2 = () => {
  const { ref, inView } = useInView({
    threshold: 0.1,
    triggerOnce: true,
  });
  const appLists = [
    {
      id: 0,
      title: "Өөрт ойр PP Box-уудыг харах, хайх",
    },
    {
      id: 1,
      title: "Захиалгуудынхаа төлвийг хянах боломжтой",
    },
  ];
  return (
    <div
      ref={ref}
      className={
        "grid grid-cols-1 md:grid-cols-2 gap-[40px] justify-center items-center mt-[40px] md:mt-0"
      }
    >
      <div className={`${
        inView
          ? "opacity-100"
          : "opacity-0"
      } transition-all duration-[2000ms] grid gap-[20px] justify-center`}>
        <div
          className={
            `grid place-items-center items-center justify-center gap-[6px] flex-shrink-0`
          }
        >
          <Image
            alt={"Download our App"}
            height={107}
            width={107}
            src={"/assets/images/app-qr.png"}
          />
          <h1 className={"font-bold text-[16px] text-white uppercase"}>
            Апп татах
          </h1>
        </div>
        <div className={"grid gap-[10px]"}>
          <h1 className={"text-white font-bold text-[30px] leading-[36px]"}>
            PP BOX АППЛИКЕЙШН
          </h1>
          {appLists && (
            <div
              className={
                "flex flex-col text-[14px] leading-[18px] text-[rgb(98,98,98)]"
              }
            >
              {appLists.map((item, index) => {
                return (
                  <div key={item.id} className={"grid grid-cols-[1fr_100%]"}>
                    <CheckCircleFilled
                      className={"text-white mt-[4px]"}
                      size={14}
                    />
                    <span
                      className={
                        "ml-[6px] text-white text-[16px] leading-[1.5em]"
                      }
                    >
                      {item.title}
                    </span>
                  </div>
                );
              })}
            </div>
          )}
        </div>
      </div>
      <Image
        className={`transition-all duration-[2000ms] object-contain ${
          inView ? "opacity-100 translate-x-0" : "opacity-0 translate-x-[100px]"
        }`}
        alt={"App showcase"}
        width={348}
        height={388}
        src={"/assets/images/app-showcase.png"}
      />
    </div>
  );
};

export default PpBoxCard2;
