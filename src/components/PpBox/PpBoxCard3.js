import { CheckCircleFilled } from "@ant-design/icons";
import { useInView } from "react-intersection-observer";

const PpBoxCard3 = () => {
  const { ref, inView } = useInView({
    threshold: 0.1,
    triggerOnce: true,
  });
  const boxLists = [
    {
      id: 0,
      title: "Хотын 200 байршлын PP Box-ын дэлгэцээр",
    },
    {
      id: 1,
      title: "Өдрийн 24,000 удаагийн давтамжтай",
    },
  ];
  return (
    <div
      ref={ref}
      className={
        "grid grid-cols-1 md:grid-cols-2 place-items-center items-center gap-[20px] md:gap-[60px] mt-[40px] md:mt-0"
      }
    >
      <div
        className={`${
          inView
            ? "opacity-100 translate-x-0"
            : "opacity-0 translate-x-[-100px]"
        } transition-all duration-1000 w-[350px] h-[350px] rounded-[40px] flex-shrink-0 p-[8px] bg-[#7dc2e4]`}
      >
        <video className={"rounded-[32px]"} controls loop autoPlay muted>
          <source src={"/assets/videos/ppbox.mp4"} type="video/mp4" />
          Your browser does not support the video tag.
        </video>
      </div>
      <div className={`${
        inView
          ? "opacity-100"
          : "opacity-0"
      } transition-all duration-[2000ms] flex flex-col gap-[10px]`}>
        <h1 className={"text-white font-bold text-[30px] leading-[36px]"}>
          БИЗНЕСЭЭ СУРТАЛЧИЛМААР БАЙНА УУ?
        </h1>
        {boxLists && (
          <div
            className={
              "flex flex-col text-[14px] leading-[18px] text-[rgb(98,98,98)]"
            }
          >
            {boxLists.map((item) => {
              return (
                <div key={item.id} className={"grid grid-cols-[1fr_100%]"}>
                  <CheckCircleFilled
                    className={"text-white mt-[4px]"}
                    size={14}
                  />
                  <span
                    className={
                      "ml-[6px] text-white text-[16px] leading-[1.5em]"
                    }
                  >
                    {item.title}
                  </span>
                </div>
              );
            })}
          </div>
        )}
      </div>
    </div>
  );
};

export default PpBoxCard3;
