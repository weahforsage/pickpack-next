import Image from "next/image";
import {Carousel} from "antd";
import {useRef} from "react";
import {carouselDats} from "../../../utils/consts";

const PpBoxCarousel = () => {
  const sliderRef = useRef(null);

  const NextArrow = props => {
    const {className, style, onClick} = props
    return (
      <div
        className={className}
        style={{...style}}
        onClick={onClick}
      />
    )
  }

  const PrevArrow = props => {
    const {className, style, onClick} = props
    return (
      <div
        className={className}
        style={{...style}}
        onClick={onClick}
      />
    )
  }
  const beforeChange = (prev, next) => {
    const prevSlideElement = sliderRef.current.innerSlider.list.querySelector(`[data-index="${prev}"] .sliderTextContent`);
    const nextSlideElement = sliderRef.current.innerSlider.list.querySelector(`[data-index="${next}"] .sliderTextContent`);
    setTimeout(() => {
      prevSlideElement.classList.remove('next-slide-anim');
      nextSlideElement.classList.add('next-slide-anim');
    });
  }
  const settings = {
    nextArrow: <NextArrow/>,
    prevArrow: <PrevArrow/>,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    autoplay: true,
    beforeChange,
    autoplaySpeed: 5000,
    infinite: true,
  }
  return (
    <Carousel
      ref={sliderRef}
      {...settings}
      className={"absolute"}
      effect={"fade"}
      autoplay
      arrows dots={false}
    >
      {carouselDats.map((item) => {
        return (
          <div className={"relative flex items-center justify-center w-full h-[620px]"} key={item.id}>
            <div
              className={"sliderTextContent transition-all opacity-0 flex flex-col z-[1] items-center justify-center relative top-[70px]"}>
              <p className={"text-white font-light text-[26px]"}>{item?.subTitle}</p>
              <h1 className={"text-white text-[40px] font-bold text-center leading-none"}>{item.title}</h1>
            </div>
            <Image priority className={"object-cover"} layout={"fill"} alt={""} src={item.image}/>
          </div>
        )
      })}
    </Carousel>
  );
};

export default PpBoxCarousel;