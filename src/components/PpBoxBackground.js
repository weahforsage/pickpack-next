import Image from "next/image";

const PpBoxBackground = () => {
  return (
    <div>
      <div
        className={
          "absolute opacity-50 top-[-125px] left-[40%] w-[350px] h-[350px]"
        }
      >
        <Image
          alt={""}
          width={350}
          height={350}
          src={"/assets/images/pp-bg-logo.svg"}
        />
      </div>
      <div
        className={"absolute opacity-50 top-0 left-[60%] w-[350px] h-[350px]"}
      >
        <Image
          alt={""}
          width={350}
          height={350}
          src={"/assets/images/pp-bg-logo.svg"}
        />
      </div>
      <div
        className={
          "absolute opacity-50 top-[50%] left-[20%] w-[350px] h-[350px]"
        }
      >
        <Image
          alt={""}
          width={350}
          height={350}
          src={"/assets/images/pp-bg-logo.svg"}
        />
      </div>
      <div
        className={
          "absolute opacity-50 top-[40%] left-[50%] w-[350px] h-[350px]"
        }
      >
        <Image
          alt={""}
          width={350}
          height={350}
          src={"/assets/images/pp-bg-logo.svg"}
        />
      </div>
      <div
        className={
          "absolute opacity-50 top-[70%] left-[40%] w-[350px] h-[350px]"
        }
      >
        <Image
          alt={""}
          width={350}
          height={350}
          src={"/assets/images/pp-bg-logo.svg"}
        />
      </div>
    </div>
  );
};

export default PpBoxBackground;
