import ViewMoreButton from "./Button/ViewMoreButton";
import Link from "next/link";
import { useInView } from "react-intersection-observer";

export default function PricingBanner() {
  const { ref, inView } = useInView({
    threshold: 0.1,
    triggerOnce: true,
  });
  return (
    <section
      ref={ref}
      className={"md:min-h-[226px] min-h-max h-full bg-[rgb(50,65,88)] p-[20px]"}
    >
      <div
        className={
          "flex max-w-[768px] mx-auto flex-col md:flex-row md:justify-between items-center h-full md:min-h-[226px] min-h-max"
        }
      >
        <h1
          className={`${
            inView
              ? "opacity-100 translate-y-0"
              : "opacity-0 translate-y-[100px]"
          } transition-all duration-1000 max-w-[452px] font-bold uppercase text-center md:text-left leading:[24px] md:leading-[38px] text-[22px] md:text-[32px] text-white`}
        >
          ЛОЖИСТИКИЙН ЦОГЦ ШИЙДЭЛ ХАЙЖ БАЙНА УУ?
        </h1>
        <Link href={"/pricing"}>
          <a>
            <ViewMoreButton
              className={`${
                inView
                  ? "opacity-100 translate-x-0"
                  : "opacity-0 translate-x-[100px]"
              } transition-all duration-1000 my-[20px] bg-pickpack-orange font-bold text-[16px]`}
            >
              Үнийн санал авах
            </ViewMoreButton>
          </a>
        </Link>
      </div>
    </section>
  );
}
