import Image from "next/image";
import { processData } from "../utils/consts";
import { ArrowRightOutlined } from "@ant-design/icons";

export default function Process() {
  return (
    <div className={"bg-[rgb(250,250,250)] flex items-center justify-center h-[120px] md:h-[255px] w-full"}>
      <div
        className={
          "flex flex-row justify-center items-center h-full"
        }
      >
        {processData.map((item, index) => {
          return (
            <div className={"flex flex-row items-center justify-center mr-[20px] last:mr-0 md:mr-[40px]"} key={item.id}>
              <div
                key={item.id}
                className={
                  "flex flex-row items-center justify-center last:mr-0 mr-[20px] md:mr-[40px]"
                }
              >
                <div className={"flex flex-col items-center"}>
                  <div className={"flex-shrink-0 relative w-[40px] h-[40px] md:w-[99px] md:h-[99px]"}>
                    <Image
                      className={"object-contain"}
                      layout={"raw"}
                      alt=""
                      src={item.image}
                    />
                  </div>
                  <p
                    className={
                      "text-pickpack-primary text-[14px] md:text-[24px] font-semibold"
                    }
                  >
                    {item.text}
                  </p>
                </div>
              </div>
              {(index === 0 || index === 1) && (
                <div
                  className={
                    "flex items-center justify-center"
                  }
                >
                  <ArrowRightOutlined className={"text-[16px] md:text-[24px]"} />
                </div>
              )}
            </div>
          );
        })}
      </div>
    </div>
  );
}
