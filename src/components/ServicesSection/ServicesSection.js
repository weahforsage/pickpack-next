import ImageCard from "../ImageCard";
import Process from "../Process";
import angilalt from "../../../public/assets/images/angilalt.jpg";

export default function ServicesSection() {
  return (
    <section>
      <Process />
      <ImageCard
        buttonRoute={"/services"}
        image={angilalt}
        lists={[
          "Хэрэглэснээрээ төлөх төлбөрийн уян хатан нөхцөл",
          "Хүн хайж ажиллуулахгүй мэргэжлийн багт ажлаа даатгаарай",
        ]}
        title={"Бүрэн даатгалтай агуулах"}
        type={"right"}
      />
    </section>
  );
}
