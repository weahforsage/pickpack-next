import { statistics } from "../../utils/consts";
import { useInView } from "react-intersection-observer";

export default function Statistics() {
  const { ref, inView } = useInView({
    threshold: 0.1,
    triggerOnce: true,
  });
  return (
    <section ref={ref}>
      <div
        className={
          "flex min-h-[186px] p-[20px] items-center justify-center h-full w-full bg-[rgb(246,246,246)]"
        }
      >
        <div
          className={`${
            inView
              ? "opacity-100 translate-y-0"
              : "opacity-0 translate-y-[100px]"
          } 
        transition-all duration-1000 flex flex-col md:flex-row flex-grow items-center md:items-baseline justify-center`}
        >
          {statistics.map((item) => {
            return (
              <div
                key={item.id}
                className={
                  "flex flex-col my-[10px] md:my-0 mx-0 md:mx-[20px] items-center max-w-[180px] text-center h-full"
                }
              >
                <h1
                  className={
                    "text-pickpack-orange text-[24px] lg:text-[44px] font-bold"
                  }
                >
                  {item.title}
                </h1>
                <p className={"text-[#626262] break-words"}>
                  {item.description}
                </p>
              </div>
            );
          })}
        </div>
      </div>
    </section>
  );
}
