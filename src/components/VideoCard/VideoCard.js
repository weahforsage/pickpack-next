import Description from "../Description";
import { useInView } from "react-intersection-observer";
export default function VideoCard({
  title,
  lists,
  videoUrl,
  videoClass,
  buttonRoute,
}) {
  const { ref, inView } = useInView({
    threshold: 0.1,
    triggerOnce: true,
  });
  return (
    <section ref={ref}>
      <div
        className={`overflow-hidden flex flex-col p-[20px] md:flex-row items-center min-h-[460px] max-w-[860px] mx-auto justify-between`}
      >
        <Description
          inView={inView}
          buttonRoute={buttonRoute}
          title={title}
          lists={lists}
        />

        <video
          className={`transition-all duration-[1200ms] mt-[30px] ${
            inView
              ? "opacity-100 translate-x-0"
              : "opacity-0 translate-x-[100px]"
          } ${videoClass ? videoClass : ""}`}
          loop
          autoPlay
          muted
          controls
        >
          <source src={videoUrl} type="video/mp4" />
          Your browser does not support the video tag.
        </video>
      </div>
    </section>
  );
}
