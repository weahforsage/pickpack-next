import boxGif from "../../public/assets/images/1.gif";
import carGif from "../../public/assets/images/2.gif";
import squaresGif from "../../public/assets/images/3.gif";

export const menuData = [
  {
    id: 0,
    title: "БИДНИЙ ТУХАЙ",
    route: "/about",
  },
  {
    id: 1,
    title: "ҮЙЛЧИЛГЭЭ",
    route: "/services",
  },
  {
    id: 2,
    title: "ҮНЭ",
    route: "/pricing",
  },
  {
    id: 3,
    title: "PP BOX",
    route: "/pp-box",
  },
  {
    id: 4,
    title: "ХОЛБОО БАРИХ",
    route: "/contact",
  },
];
export const processData = [
  {
    id: 0,
    image: squaresGif,
    text: "Агуулалт",
  },
  {
    id: 1,
    image: boxGif,
    text: "Ангилалт",
  },
  {
    id: 2,
    image: carGif,
    text: "Хүргэлт",
  },
];

export const statistics = [
  {
    id: 0,
    title: "15,000",
    description: "м3 агуулах",
  },
  {
    id: 1,
    title: "125,000",
    description: "нэр төрлийн бараа",
  },
  {
    id: 2,
    title: "25-35%",
    description: "агуулахын зардлын хэмнэлт",
  },
  {
    id: 3,
    title: "40-50%",
    description: "хүний нөөцийн зардлын хэмнэлт",
  },
];

export const cards = [
  {
    id: 0,
    imagePath: "/assets/images/uilchilgee-standart.jpg",
    title: "Стандарт үйлчилгээ",
    description:
      "Таны барааг таны хүссэн газраас татан аваад агуулах дээрээ байршуулан захиалгын дагуу хэрэглэгчид хүргэж өгөх цогц үйлчилгээ.",
    color: "bg-[#324158]",
  },
  {
    id: 1,
    imagePath: "/assets/images/hover-aguulah.jpg",
    title: "Агуулахын үйлчилгээ",
    description:
      "Хэрэглэснээрээ төлөх уян хатан төлбөрийн нөхцөлтэйгөөр өөрийн хэрэгцээ шаардлагад тохируулан агуулахын үйлчилгээ аваарай.",
    color: "bg-pickpack-orange",
  },
  {
    id: 2,
    imagePath: "/assets/images/hover-hurgelt.jpg",
    title: "Хүргэлтийн үйлчилгээ",
    description:
      "Өдөртөө бараа татан авалттай энгийн болон том хэмжээтэй барааны хүргэлтийн үйлчилгээ.",
    color: "bg-[#40C0F2]",
  },
  {
    id: 3,
    imagePath: "/assets/images/hover-B2B.jpg",
    title: "B2B үйлчилгээ",
    description:
      "Таны барааг таны хүссэн газраас татан аваад агуулах дээрээ байршуулан, хуваарийн дагуу түгээх цогц шийдэл.",
    color: "bg-[#324158]",
  },
];

export const carouselDats = [
  {
    id: 0,
    title: "ТАНЫ ШИНЭ 24/7 ХАЯГ",
    image: "/assets/images/slider-1.png"
  },
  {
    id: 1,
    title: "ХОТЫН 200 БАЙРШИЛД",
    image: "/assets/images/slider-2.png"
  },
  {
    id: 2,
    title: "АВ • ХАДГАЛ • ИЛГЭЭ",
    subTitle: "PP BOX-ООР",
    image: "/assets/images/slider-3.png"
  }
]
