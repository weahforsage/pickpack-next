module.exports = {
  content: ["./pages/**/*.{js,jsx}", "./src/components/**/*.{js,jsx}"],
  theme: {
    extend: {
      fontFamily: {
        gilroy: ["Gilroy"],
      },
      colors: {
        pickpack: {
          primary: "rgb(40 26 57)",
          orange: "#fd7524",
        },
      },
    },
  },
  plugins: [],
};
